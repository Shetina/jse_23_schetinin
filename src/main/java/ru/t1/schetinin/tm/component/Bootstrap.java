package ru.t1.schetinin.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.schetinin.tm.api.repository.ICommandRepository;
import ru.t1.schetinin.tm.api.repository.IProjectRepository;
import ru.t1.schetinin.tm.api.repository.ITaskRepository;
import ru.t1.schetinin.tm.api.repository.IUserRepository;
import ru.t1.schetinin.tm.api.service.*;
import ru.t1.schetinin.tm.command.AbstractCommand;
import ru.t1.schetinin.tm.command.project.*;
import ru.t1.schetinin.tm.command.system.*;
import ru.t1.schetinin.tm.command.task.*;
import ru.t1.schetinin.tm.command.user.*;
import ru.t1.schetinin.tm.enumerated.Role;
import ru.t1.schetinin.tm.enumerated.Status;
import ru.t1.schetinin.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.schetinin.tm.exception.system.CommandNotSupportedException;
import ru.t1.schetinin.tm.model.Project;
import ru.t1.schetinin.tm.model.User;
import ru.t1.schetinin.tm.repository.CommandRepository;
import ru.t1.schetinin.tm.repository.ProjectRepository;
import ru.t1.schetinin.tm.repository.TaskRepository;
import ru.t1.schetinin.tm.repository.UserRepository;
import ru.t1.schetinin.tm.service.*;
import ru.t1.schetinin.tm.util.TerminalUtil;

@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, projectRepository, taskRepository);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(userService);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    {
        registry(new ApplicationHelpCommand());
        registry(new ApplicationVersionCommand());
        registry(new ApplicationAboutCommand());
        registry(new SystemInfoCommand());

        registry(new ProjectListCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectUpdateByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectCompleteByIdCommand());

        registry(new TaskListCommand());
        registry(new TaskCreateCommand());
        registry(new TaskClearCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskBindToProjectCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new TaskShowByProjectIdCommand());

        registry(new UserLoginCommand());
        registry(new UserRegistryCommand());
        registry(new UserViewProfileCommand());
        registry(new UserUpdateProfileCommand());
        registry(new UserChangePasswordCommand());
        registry(new UserLogoutCommand());
        registry(new UserLockCommand());
        registry(new UserUnlockCommand());
        registry(new UserRemoveCommand());

        registry(new ApplicationExitCommand());
    }

    private void processCommands() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND: ");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("OK");
                loggerService.command(command);
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    private void initLogger() {
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
        Runtime.getRuntime().addShutdownHook(new Thread(() -> loggerService.info("*** TASK MANAGER IS SHOOTING DOWN ***")));
    }

    private void processArguments(@Nullable final String[] arguments) {
        if (arguments == null || arguments.length < 1) return;
        processArgument(arguments[0]);
        exit();
    }

    private void exit() {
        System.exit(0);
    }

    private void initDemoData() {
        @NotNull final User test = userService.create("test", "test", "test@test.ru");
        @NotNull final User user = userService.create("user", "user", "user@user.ru");
        @NotNull final User admin = userService.create("admin", "admin", Role.ADMIN);

        projectService.add(test.getId(), new Project("TEST PROJECT", Status.IN_PROGRESS));
        projectService.add(test.getId(), new Project("DEMO PROJECT", Status.NOT_STARTED));
        projectService.add(test.getId(), new Project("BEST PROJECT", Status.IN_PROGRESS));
        projectService.add(test.getId(), new Project("BETA PROJECT", Status.COMPLETED));
        projectService.add(user.getId(), new Project("USER PROJECT", Status.COMPLETED));
        projectService.add(admin.getId(), new Project("ADMIN PROJECT", Status.COMPLETED));
        
        taskService.create(test.getId(), "MEGA TASK");
        taskService.create(test.getId(), "BETA TASK");
        taskService.create(test.getId(), "SUPER TASK");
        taskService.create(test.getId(), "CLEAN TASK");
    }

    private void processCommand(@Nullable final String command) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private void processArgument(@Nullable final String argument) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    private void registry(@NotNull final AbstractCommand command){
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void run(@Nullable final String[] args) {
        processArguments(args);
        initDemoData();
        initLogger();
        processCommands();
    }

}